Hi there! Before you get started, you should know three crucial things.

**First**, please read the rules. A simplified version is in the sidebar. A more detailed version is [here](http://www.reddit.com/r/financialindependence/wiki/rules).

**Second**, if you want instant gratification then please **use the search bar before posting!!!** The odds are good that your question/subject/etc. has already been discussed at length—especially if you’re asking a “Should I?” question. Such topics include:

* Running rental property.

* Side hustles and other forms of entrepreneurship.

* Career path.

* What’s your number?

* The 4% rule.

* Paying down debt, including student loans and mortgages.

* Getting your SO on board.

* Budgeting.

* The “best” investments.

* Cost-of-living estimates for different cities/regions/countries/etc.

* Allocation mixes.

* Education.

* The most FIRE-friendly places to live.

* What’s your FIRE number?

* Online tools/calculators/etc.

* Success stories.

* FIRE on an “ordinary” salary and/or self-employment.

Many of these questions are also covered below.

**Third**, if you want advice that’s more specific to your situation, check out the weekly “Help Me FIRE!” thread that’s stickied to the top of the front page.

And with all that said, good luck on your FIRE journey!

----

#The Basics

##What is financial independence (FI)?

It is typically defined as having enough income (from investments, passive businesses, real estate, etc) to pay for your *reasonable* living expenses for the rest of your life. You have the freedom to do what you want with your time (within reason). Working (full or part time), hobbies which generate income, or other activities are optional at this point.

This specific subreddit focuses on the three knobs controlling getting you to financial independence quickly. Reducing expenses, increasing income, and investing.

* **Reducing expenses** - Reducing your living expenses to a reasonable level is the most critical step. Five-star restaurants, vacation homes, and jets are generally not achievable as a reasonable financial independence goal. But if you can realize contentment with significantly lower expenses, your ability to reach financial independence is greatly improved.
* **Increasing income** - Forget get rich quick schemes. But many of those on Reddit are early (or before) their career. There are plenty of those underemployed who should look into steps to improve their financial situation. There are choices each of us can make to increase our ability to generate income. Improve your education, ask for a raise, create a side business.
* **Investing** - When you're heading towards financial independence, you need your money to work for you. While we don't focus on specific strategies (most here would aim for passive index fund investing), we're interested in long term sustainable investment returns. Money in a savings account won't grow at the necessary rate. Note that if you have questions about real estate, financial investments (e.g., stocks, index funds, crypto), starting businesses, etc. you should ask them on other subreddits like /r/realestate, /r/investing, /r/entrepreneurship, and so forth.

## I am XX years old and want to be financially independent, where do I start?

The basic tenet of FI is to spend less than you earn and invest the difference into things that earn money for you.

**Spend Less/Save More**

1. Minimize taxes to the full extent of the law. Look into 401k/403b/457/IRA/HSA accounts as some examples.

2. Reduce your living expenses. Smaller homes, apartments, rent a room. Avoid lifestyle creep as your income (hopefully) rises over time.

3. Reduce transportation costs. Live close to work if possible. Avoid expensive cars - Consider bicycles, used cars, public transportation, etc.

4. Learn self-sufficiency skills. Beyond the personal satisfaction in achieving something, you'll save money. Consider cooking, mending clothes, home/automobile/bike repair, woodworking, gardening, etc.

**Earn More** 

1. If you're in school (whether that's high school, trade school, or college/university), focus on that first. It's easier to be FI on $50k+/yr than minimum wage. Look carefully at your career path. Is this degree going to pay you back appropriately in the future?

2. If you have a job, work more/harder/better/smarter. If you have the time, consider working extra hours and/or accepting challenging and long-distance assignments (family commitments permitting).

3. Negotiate to get paid what you're worth (see point 2 above)

**Be careful about long term decisions**

"Future you" may be interested in FI. Part of FI is the flexibility of having choices in the future. Be careful about locking yourself down.

1. If you're not positive you want to live in an area, avoid buying a house. Consider renting. Significantly easier to change your mind later.

2. Many marriages end in divorce. Divorce is expensive. Do what you can to avoid this.

3. Consider what careers are flexible and which aren't. Nursing is known as a great career choice for people who move around (there is a need for local nurses in almost every major city in the world). Software engineering can often be done remotely. Other career choices may have less flexibility in location / availability (e.g. stock broker).

4. Any loan means you are borrowing from "future you". Be very careful about making your future self pay back a lot of money. Loans for depreciating assets (cars, electronics, etc) in particular are generally bad ideas.

## Where can I learn more about FI (other than this subreddit)?

One of the original texts for the FI movement is [Your Money or Your Life](http://smile.amazon.com/Your-Money-Life-Transforming-Relationship-ebook/dp/B0052MD8VO/). Buy a copy, or better yet, rent it from your local library.

The primary writers on FI today are largely bloggers. The sidebar of /r/financialindependence has a number of popular blogs listed. We periodically add new sources of information as they become available.

A couple specific reads:

* [Early Retirement Extreme 21-day makeover](http://earlyretirementextreme.com/day-0-the-early-retirement-extreme-30-day-makeover.html)
* [Mr. Money Mustache Zero to Hero](http://www.mrmoneymustache.com/2013/02/22/getting-rich-from-zero-to-hero-in-one-blog-post/) 

Of course, there are also great resources elsewhere on Reddit. /r/personalfinance and /r/frugal are very helpful for learning to manage your finances and acquire the frugal skills that are part of the FI mindset.

## How do I calculate my savings rate?

Short answer: We recommend people use - **Savings / (Gross - Taxes)**

The Bureau of Economic Analysis formula for "personal savings rate" is described below:

http://www.investopedia.com/university/releases/personalconsumption.asp

Basically, personal savings rate is defined as (disposable personal income minus personal outlay) divided by disposable personal income. What does this mean? Here is one way to think about it.

gross = taxes + spending + saving

gross - taxes = spending + saving

gross - taxes - spending = saving

The BEA uses "disposable personal income" as their phrase for (gross - taxes). They use "personal outlay" to describe spending. So they phrase personal savings rate as (DPI - personal outlay)/DPI.

Their definition of personal savings rate puts (gross - taxes) in the denominator. To express it using the more simple words:

savings rate = [(gross - tax) - (spending)]/(gross - tax), or (saving)/(gross - tax).

So the BEA has chosen to report personal savings rate as describing essentially what people save out of what they might possibly save after taxes. Thus, someone could theoretically have a 100% savings rate if they had no spending, even though taxes were taken out of their gross.

## I'd like some specific advice. How can I go about posting on this subreddit?

When you're posting &amp; asking for information, please look at this checklist (**Copy and paste it into your post**) and ensure you're giving the subscribers everything necessary to give you their valuable (or sometimes not valuable) advice. Skip a bit of data if you're really sure it's not applicable, but these are often useful to know. **If your question is more of a general Personal Finance question (like "What is the 401k contribution limit again?" or "What AA should I have if I want to retire in 20 years?"), then please post this in the Daily Discussion Thread**

**Life Situation:** IRS filing status, number &amp; ages of dependents, and anything else (state/country of residence, age, etc.) you are comfortable sharing.

**FIRE Progress:** Provide details about where you are on the road to FIRE. If possible, provide expected FI/ER dates.

**Gross Salary/Wages:** Before any deductions

**Yearly Savings Amounts:** 401k, HSA, FSA, IRA, insurance, etc. - whatever you have

**Other Ordinary Income:** Provide sources and any relevant details, the more the better

**Rental Income, Actual Expenses, and Depreciation:** If these are significant for you

**Current expenses:** Provide breakdown and relevant details. 

For mortgage payments, separate the P&amp;I (which stop when the mortgage is paid) from the T&amp;I (and anything else) which continue as long as you own the property.

**Expected ER expenses:** (optional, if relevant)

**Assets:** Amount &amp; description - include current asset allocation plan if you have one

**Liabilities:** Description, original loan amount, rate, original length, and monthly payment (which should be consistent with a spreadsheet PMT calculation).  Add current balance and time remaining if close to final payment.

**Specific Question(s):** Providing a detailed breakdown is important, so is asking for specific information so we know what kind of help/advice you are looking for.

----

#Investing

## How much do I need to save to retire?

The short answer: 25 times your annual spending (with caveats)

The long(ish) answer: In 1998, three professors at Trinity University released what became known as the [Trinity Study](http://en.wikipedia.org/wiki/Trinity_study). The study examined the U.S. stock and bond markets over every 15-30 year return period between 1925 and 1995 (the data was recently refreshed in 2009). They concluded that by starting with 4% of your portfolio, and withdrawing that amount (increasing yearly with inflation) every year, you would have a 96% chance to not run out of money during a 30 year period.

* Assumes only a 30 year retirement period. Longer retirements likely need a lower withdrawal rate.
* Assumes a mixture of stocks and bonds
* Assumes $1 in the bank account is "success". So some 30 year periods had lower ending balances.

4% became known as the “Safe Withdrawal Rate” (SWR). The nature of the stock market (and historical returns) means that in most cases, the portfolio grew faster than the withdrawal rate. 4% of a portfolio is the amount you can withdraw, or reversing the math, 25x your withdrawal amount is equal to the amount you need to save.

Examples:

* I need $40k in retirement. Therefore I should save (at least) $40k*25 = $1M
* I have $1M in my retirement accounts. Therefore I can spend $40k yearly ($1M * 4%) for ~30 years.

A couple great articles on this topic

* [The Shockingly Simple Math Behind Early Retirement](http://www.mrmoneymustache.com/2012/01/13/the-shockingly-simple-math-behind-early-retirement/)
* [Safe Withdrawal Rate - Calculations by the Mad Fientist](http://www.madfientist.com/safe-withdrawal-rate/)

## I'm concerned about the potential for poor investment returns in the future considering the current market..

People are always worried that the future market will not perform as well as it did in the past (for a variety of reasons which always change). However, we have tools available which increase our chances of staying financially independent:

* Working an enjoyable side job after retirement (many early retirees have more than enough time and energy to get paid to do something they enjoy). Even small amounts of income can drastically decrease your investment withdrawals.
* Decreasing spending with increasing age (a generally true phenomenon).
* Social Security, Medicare, or pensions. Young retirees often don't take these into account, but they're likely to contribute some amount of returns as you age.
* Reducing your spending in lean times as necessary.

By taking some of these commonplace assumptions into account, we can be less conservative with the SWR, and use 4% instead of something lower.

## What about inflation?

The Trinity Study took inflation into account, but it bears repeating: $200,000 now is not the same as $200,000 10 or 20 years from now. Inflation will eat away at your returns at an average rate of about 3% per year (historically it was ~4%, but inflation has been lower for recent years). That is why FIers tend to invest heavily in stocks instead of lower-yielding financial instruments - the average gains of the stock market have historically outpaced inflation. The average annual increase in the value of the U.S. stock market has been 9.5% (various values are found online, this seems the most accurate.. a geometric average since 1928). If you subtract off ~4% lost to inflation historically, you'd see that leaves you 5.5% growth on average. Assuming average (or better years), you should expect your investment value (after inflation) to increase over long periods time. Given the volatility of the market, your actual experience will be likely much better or much worse than that calculation in the short run.

## What about retirements longer than 30 years?

You can greatly increase your chances of "success" (not going broke) through the following:

* Avoiding investment mistakes (e.g. making the mistake of withdrawing your investments during a market crash)
* Saving more than 25x your expenses. Decreasing your withdrawal rate increases your chances for success.
* Decreasing your expenses in retirement. Withdrawing less than your expected expenses would increase your success.
* Being flexible with your yearly expenses. Studies have shown that decreasing your expenses during a market crash drastically increases success rates.
* Ensuring your money is fully invested. Too much held in cash will reduce your return rates, which reduces your ability to fight against inflation over time.

## I have $[X] sitting around, and want to be FI. What should I do with it?

See [this article from from the /r/personalfinance FAQ](http://www.reddit.com/r/personalfinance/wiki/commontopics).

Many people on the sub advocate an indexing strategy rather than investing in individual stocks. A few options below:

* [Vanguard's Target Retirement Funds (Can rely on a single fund)](https://investor.vanguard.com/mutual-funds/target-retirement/#/)
* [Vanguard's Lifestrategy Funds (Can rely on a single fund)](https://investor.vanguard.com/mutual-funds/lifestrategy/#/)
* [DIY 3-Fund Portfolio](https://www.bogleheads.org/wiki/Three-fund_portfolio).

There was also a popular post on this subreddit which mentioned [commercial real estate](http://www.reddit.com/r/financialindependence/wiki/commercialrealestatepost).

## More specifically, what about this investment strategies? Is this fund better than this other one?

We try to avoid investment discussions here. The recommended path for savings is located in the [/r/personalfinance FAQ](http://www.reddit.com/r/personalfinance/wiki/commontopics). There are always exceptions, but this path will work for most people.

If you have more detailed questions around investing which are not Financial Independence specific questions, please head over to /r/personalfinance or /r/investing.

----

#Tax advantaged accounts

## How do 401k / IRAs (and other tax-deductible accounts) help me? When should I decide to use them?

In general, if your spending before retirement is less than your earnings (by definition, this should be everyone planning on becoming FI), a tax deductible investment account will benefit you in the long run.

Lets assume you're going to maintain your lifestyle, same level of spending. You're also heading for FI, so you're saving a lot. Imagine you're making $100k gross, you're being taxed some percentage, you're spending $50k and saving the remainder.

Tax brackets  - http://www.bankrate.com/finance/taxes/tax-brackets.aspx - We will ignore tax complexity of deductions / etc.. in the end the lesson is the same.

As you can see from the above link, assuming you're married and making $100k total, you have money being taxed in the 10, 15, and 25% tax brackets. ~25k of which is in the 25% tax bracket.

If you save $10k in your 401k, you can deduct that from your taxable income. You now have 15k in the 25% tax bracket. On that $10k, you didn't have to pay your 25% tax rate. Or, you saved ~$2,500 in taxes.

Now zoom forward 15 years, and you're retired (ignoring inflation to keep things simple). Assuming you maintain the same lifestyle, you need to withdraw $50k per year from your accounts still.

If you withdrew $50k from your 401k, your income is now $50k (as 401k withdrawals are counted as income). You have money being taxed in the 10%, and 15% tax bracket. You're no longer being taxed in the 25% bracket at all. Hey look at that! That money you previously were being taxed 25% on is now being taxed at 15%. You'll never need to pay that extra tax.

While this could be a 50 page document, in the end it's relatively simple. If you're saving money, you are most likely paying into higher tax brackets than you will in retirement. So if you can pay taxes later, that's better for you :)

## But I want to retire early, should I really use tax advantaged accounts? Because I'm locked in aren't I?

Short answer, no, you're not locked in. A regularly repeated concern is that 59 1/2 is the standard age in the US for 401K/IRA withdrawals. However, the tax advantages in these accounts shouldn't be missed. There are a few mechanisms outlined below to access that money early.

* [MMM: How much is too much in your 401K?](http://www.mrmoneymustache.com/2011/11/11/how-much-is-too-much-in-your-401k/)
* [jlcollinsnh: Early Retirement Withdrawal Strategies and Roth Conversion Ladders](http://jlcollinsnh.com/2013/12/05/stocks-part-xx-early-retirement-withdrawal-strategies-and-roth-conversion-ladders-from-a-mad-fientist/)
* [madFIentist: Retire Even Earlier Without Earning More or Spending Less](http://www.madfientist.com/retire-even-earlier/)

## Should I use a Traditional or Roth IRA/401(k)?

Some links on this topic:

* [Reddit post regarding after-tax 401(k)s](http://www.reddit.com/r/financialindependence/comments/2l9wnm/faq_submission_my_company_offers_an_aftertax_401k/)
* [madFIentist: Traditional IRA vs. Roth IRA – The Final Battle](http://www.madfientist.com/traditional-ira-vs-roth-ira/)
* [The Case Against Roth 401(k)](http://thefinancebuff.com/case-against-roth-401k.html)